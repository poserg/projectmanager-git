# -*- coding: utf8 -*-
from django.db import models
from django.contrib import admin
from django.contrib.auth.models import User

name_max_length = 150

class Object(models.Model):
    name = models.CharField(max_length=name_max_length)
    
    def __unicode__(self):
        return self.name

    class Meta:
        ordering = ['name']

class Company(Object):
    """Company of users"""
    description = models.TextField()
    address = models.TextField()
    email = models.EmailField()
    telephone = models.CharField(max_length=10)
    person = models.CharField(max_length=name_max_length)
    
    class Meta:
        verbose_name_plural = 'Companies'
        
class CompanyAdmin(admin.ModelAdmin):
    list_display = ('name', 'description', 'address',
                    'email', 'telephone', 'person')
    list_filter = ('name', 'description', 'address',
                   'email', 'telephone', 'person')
    
admin.site.register(Company, CompanyAdmin)
    
class Division(Object):
    company = models.ForeignKey(Company)
    #users = models.ManyToOneRel(User)
    
class DivisionAdmin(admin.ModelAdmin):
    list_display = ('name', 'company_name')
    list_filter = ('name',)
    #filter_horizontal = ('company_name',)
    
    def company_name(self, obj):
        return obj.company.name
    
admin.site.register(Division, DivisionAdmin)

class Project(Object):
    date_start = models.DateField()
    date_finish = models.DateField()
    responsible = models.ForeignKey(User)
    
class ProjectAdmin(admin.ModelAdmin):
    list_display = ('name', 'responsible',
                    'date_start', 'date_finish')
    list_filter = ('name', 'responsible',
                   'date_start', 'date_finish')
    
admin.site.register(Project, ProjectAdmin)

class Requirement(Object):
    description = models.TextField()
    date_create = models.DateField(auto_now=True)
    project = models.ForeignKey(Project)
    
class RequirementAdmin(admin.ModelAdmin):
    list_display = ('name', 'description',
                    'date_create', 'project')
    list_filter = ('name', 'description',
                   'date_create', 'project')
    
admin.site.register(Requirement, RequirementAdmin)

class Document(Object):
    description = models.TextField()
    project = models.ForeignKey(Project)
    date_create = models.DateField(auto_now=True)
    requirement = models.ForeignKey(Requirement)
    
class DocumentAdmin(admin.ModelAdmin):
    list_display = ('name', 'description', 'project',
                    'requirement', 'date_create')
    list_filter = ('name', 'description', 'project',
                   'requirement', 'date_create')
    
admin.site.register(Document, DocumentAdmin)

class Iteration(Object):
    date_start = models.DateField()
    date_finish = models.DateField()
    project = models.ForeignKey(Project)
    
class IterationAdmin(admin.ModelAdmin):
    list_display = ('name', 'project', 'date_start',
                    'date_finish')
    list_filter = ('name', 'project', 'date_start',
                   'date_finish')
    
admin.site.register(Iteration, IterationAdmin)

class Product(Object):
    date_start = models.DateField()
    date_finish = models.DateField()
    responsible = models.ForeignKey(User)
    project = models.ForeignKey(Project)
    
class ProductAdmin(admin.ModelAdmin):
    list_display = ('name', 'responsible', 'project',
                    'date_start', 'date_finish')
    list_filter = ('name', 'responsible', 'project',
                   'date_start', 'date_finish')
    
admin.site.register(Product, ProductAdmin)

class Module(Object):
    description = models.TextField()
    date_start = models.DateField()
    date_finish = models.DateField()
    product = models.ForeignKey(Product)
    requirements = models.ManyToManyField(Requirement)
    
class ModuleAdmin(admin.ModelAdmin):
    list_display = ('name', 'description', 'product',
                    'requirement', 'date_start',
                    'date_finish')
    list_filter = ('name', 'description', 'product',
                   'date_start', 'date_finish')
    filter_horizontal = ('requirements',)
    
admin.site.register(Module, ModuleAdmin)

class Version(Object):
    description = models.TextField()
    date_create = models.DateField(auto_now=True)
    modules = models.ManyToManyField(Module)
    product = models.ForeignKey(Product)
    
class VersionAdmin(admin.ModelAdmin):
    list_display = ('name', 'description', 'product',
                    'module', 'date_create')
    list_filter = ('name', 'description', 'product',
                   'date_create')
    
admin.site.register(Version, VersionAdmin)

class Plan(Object):
    description = models.TextField()
    date_start = models.DateField()
    date_finish = models.DateField()
    product = models.ForeignKey(Product)
    
class PlanAdmin(admin.ModelAdmin):
    list_display = ('name', 'description', 'product',
                    'date_start', 'date_finish')
    list_fitler = ('name', 'description', 'product',
                   'date_start', 'date_finish')
    
admin.site.register(Plan, PlanAdmin)

class BaseTask(Object):
    date_create = models.DateField(auto_now=True)
    date_start = models.DateField()
    date_finish = models.DateField()
    product = models.ForeignKey(Product)
    version = models.ForeignKey(Version)
    plan = models.ForeignKey(Plan)
    members = models.ManyToManyField(User, through="Member")
    
#    class Meta:
#        abstract = True

class BaseTaskAdmin(admin.ModelAdmin):
#    list_display = ('name', 'product', 'version',
#                    'plan', 'date_create', 'task_members')
#    
#    def task_members(self, obj):
#        s = ''
#        for i in obj.members:
#            s += i.username
#            if i != obj.members[-1]:
#                s += ', '
#        return s
    list_display = ('name', 'product', 'version',
                    'plan', 'date_create')
    list_filter = ('name', 'product', 'version',
                   'plan', 'date_create')
        
class Status(Object):
    description = models.TextField()
    
    class Meta:
        abstract = True
        
class StatusAdmin(admin.ModelAdmin):
    list_display = ('name', 'description')
    list_filter = ('name', 'description')
        
class Task_Status(Status):
    class Meta:
        verbose_name_plural = 'Task\'s statuses'

admin.site.register(Task_Status, StatusAdmin)

class Task(BaseTask):
    status = models.ForeignKey(Task_Status)
    
class TaskAdmin(admin.ModelAdmin):
    list_display = ('name', 'product', 'version',
                    'plan', 'date_create', 'date_finish', 
                    'status') 
    list_filter = ('name', 'product', 'version',
                    'plan', 'date_create', 'date_finish', 
                    'status') 
    
admin.site.register(Task, TaskAdmin)

class Error_Status(Status):
    class Meta:
        verbose_name_plural = 'Error\'s statuses'

admin.site.register(Error_Status, StatusAdmin)

class Error(BaseTask):
    status = models.ForeignKey(Error_Status)
    
admin.site.register(Error, BaseTaskAdmin)

class Member_type(Object):
    pass

class Member(models.Model):
    user = models.ForeignKey(User)
    task = models.ForeignKey(BaseTask)
    type = models.ForeignKey(Member_type)
    
class Report(Object):
    member = models.ForeignKey(Member)
    description = models.TextField()
    date = models.DateField(auto_now=True)
    
class ReportAdmin(admin.ModelAdmin):
    list_display = ('name', 'description', 'member', 'date')
    list_filter = ('name', 'description', 'member', 'date')
    
admin.site.register(Report, ReportAdmin)
