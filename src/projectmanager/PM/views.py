# -*- coding: utf8 -*-
from django import forms
from projectmanager.PM.models import Company

class CompanyForm(forms.ModelForm):
    class Meta:
        model = Company
